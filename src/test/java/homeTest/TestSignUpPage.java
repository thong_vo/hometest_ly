package homeTest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.AddressPage;
import pages.HomePage;
import pages.LoginPage;
import pages.SignUpPage;

import java.util.concurrent.TimeUnit;

public class TestSignUpPage {

    String path = System.getProperty("user.dir");
    String driverPath = path + "\\src\\main\\resources\\driver\\chromedriver.exe";
    public WebDriver driver;

    @BeforeTest

    public void setup() {
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.utest.com/");
//        WebDriverWait wait=new WebDriverWait(driver, 20);
//        WebElement a ;
//
//       a = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@class, 'bar-header')]//*[@href='/signup/personal']")));
    }

    @Test
    public void test_Login_Function() {
        new HomePage(driver).goToSignUp();
        new SignUpPage(driver).fillSignUpForm();
        new AddressPage(driver).verifyPageIsLoaded();
    }

    @AfterTest
    public void tearDown() {
        driver.close();
    }
}
