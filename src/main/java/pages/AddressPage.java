package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class AddressPage {
    WebDriver driver;

    @FindBy(xpath="//*[contains(@class, 'bar-header')]//*[@href='/signup/personal']")
    WebElement lnk_signUp;

    public AddressPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public AddressPage verifyPageIsLoaded(){
        String url = driver.getCurrentUrl();
        Assert.assertTrue(url.contains("signup/address"), "Address page is displayed");
        return this;
    }


}
