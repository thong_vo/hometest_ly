package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class SignUpPage {

    public WebDriver driver;

    @FindBy(xpath = "//*[@id='firstName']")
    WebElement txt_firstName;

    @FindBy(xpath = "//*[@id='lastName']")
    WebElement txt_lastName;

    @FindBy(xpath = "//*[@id='email']")
    WebElement txt_email;

    @FindBy(xpath = "//*[@id='birthMonth']")
    WebElement dropdown_month;

    @FindBy(xpath = "//*[@id='birthDay']")
    WebElement dropdown_day;

    @FindBy(xpath = "//*[@id='birthYear']")
    WebElement dropdown_year;

    @FindBy(id = "genderCode")
    WebElement dropdown_gender;

    @FindBy(xpath = "//*[contains(@class, 'btn-blue')]")
    WebElement btn_next;

    @FindBy(xpath = "//*[contains(@class, 'choices-row active')]")
    WebElement row_active;

    @FindBy(xpath = "//*[@id='emailError']")
    WebElement email_error;


    public SignUpPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SignUpPage fillSignUpForm(){
        txt_firstName.sendKeys("Ly");
        txt_lastName.sendKeys("Tran");
        txt_email.sendKeys("lytran610@gmail.com");
        dropdown_month.click();
        row_active.click();
        dropdown_day.click();
        row_active.click();
        dropdown_year.click();
        row_active.click();
        dropdown_gender.click();
        row_active.click();
        btn_next.click();
        return this;
    }

    public SignUpPage fillSignUpFormInvalid(){
        txt_firstName.sendKeys("Ly");
        txt_lastName.sendKeys("Tran");
        txt_email.sendKeys("lytran610%&^%");
        dropdown_month.click();
        row_active.click();
        dropdown_day.click();
        row_active.click();
        dropdown_year.click();
        row_active.click();
        dropdown_gender.click();
        row_active.click();
        btn_next.click();
        return this;
    }

    public SignUpPage verifyEmailError(){
        Assert.assertTrue(email_error.isDisplayed(), "Email is not correct");
        return this;
    }


}
